
public class Recursives {
    /**
     * Given a string, compute recursively (no loops) the number of "11"
     * substring in the string. The "11" substrings should not overlap.
     * count11("11abc11") → 2 count11("abc11x11x11") → 3 count11("111") → 1
     */
    public static int count11(String str) {
        int count = 0;
        if (str.length() < 2) {
            return 0;
        }
        if (str.substring(str.length() - 2).equals("11")) {
            count++;
            str = str.substring(0, str.length() - 1);
        }
        return count + count11(str.substring(0, str.length() - 1));
    }

    public static int count11_2(String str) {
        int count = 0;
        if (str.length() < 2) {
            return 0;
        }
        if (str.substring(str.length() - 2).equals("11")) {
            count++;
        }
        return count + count11(str.substring(0, str.length() - 1));
    }

    /**
     * Given a string, return true if it is a nesting of zero or more pairs of
     * parenthesis, like "(())" or "((()))". Suggestion: check the first and
     * last chars, and then recur on what's inside them. nestParen("(())") →
     * true nestParen("((()))") → true nestParen("(((x))") → false
     */
    public static boolean nestParen(String str) {
        if (str.equals("()")) {
            return true;
        }
        if (str.length() < 2) {
            return false;
        }
        if (str.charAt(0) == '(' && str.charAt(str.length() - 1) == ')') {
            return nestParen(str.substring(1, str.length() - 1));
        } else {
            return false;
        }
    }

    public static boolean nestParen_2(String str) {
        if (str.equals("()") || str.startsWith("(") && str.endsWith(")")
                && str.substring(1, str.length() - 2).contains("(")
                && str.substring(1, str.length() - 2).contains(")")) {
            return true;
        }
        if (str.length() < 2) {
            return false;
        }
        if (str.charAt(0) == '(' && str.charAt(str.length() - 1) == ')') {
            return nestParen(str.substring(1, str.length() - 1));
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(count11("11abc11"));
        System.out.println(count11("abc11x11x11"));
        System.out.println(count11("111"));

        System.out.println("-".repeat(10));
        System.out.println(count11_2("11abc11"));
        System.out.println(count11_2("abc11x11x11"));
        System.out.println(count11_2("111"));

        System.out.println("-".repeat(10));
        System.out.println(nestParen("(())"));
        System.out.println(nestParen("((()))"));
        System.out.println(nestParen("(((x))"));

        System.out.println("-".repeat(10));
        System.out.println(nestParen_2("(())"));
        System.out.println(nestParen_2("((()))"));
        System.out.println(nestParen_2("(((x)))"));

    }
}
